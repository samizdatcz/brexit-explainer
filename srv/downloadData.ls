require! {
  request
  fs
}

(err, response, body) <~ request.get "https://ft-ig-brexit-polling.herokuapp.com/data.json"
fs.writeFileSync "#__dirname/../data/data.json" body
