shareUrl = window.location
d3.select \a#scroll-btn .on \click ->
    d3.event.preventDefault!
    content = document.querySelector \#content
    offset = ig.utils.offset content .top
    d3.transition!
      .duration 800
      .tween "scroll" scrollTween offset

d3.selectAll "a[target='_blank']" .on \click ->
  window.open do
    @getAttribute \href
    ''
    "width=550,height=265"

isAtTop = yes
setHeaderClass = ->
  top = (document.body.scrollTop || document.documentElement.scrollTop)
  height = window.innerHeight
  if top >= height and isAtTop
    d3.select \header .classed \at-top no
    isAtTop := no
  else if top < height and not isAtTop
    d3.select \header .classed \at-top yes
    isAtTop := yes

window.addEventListener \scroll setHeaderClass
setHeaderClass!

scrollTween = (offset) ->
  ->
    interpolate = d3.interpolateNumber do
      window.pageYOffset || document.documentElement.scrollTop
      offset
    (progress) -> window.scrollTo 0, interpolate progress
