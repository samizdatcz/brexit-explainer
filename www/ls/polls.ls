return unless ig.containers.polls
data = ig.data.polls
for datum in data.combinedData
  datum.date = new Date datum.date

for datum in data.smoothedData
  datum.date = new Date datum.date

maxY = Math.max do
  d3.max data.combinedData.map (.leave)
  d3.max data.combinedData.map (.remain)
padding = top: 20 left: 10 right: 150 bottom: 35
bigContainer = d3.select ig.containers.polls
bigContainer.append \h3 .html "Podle britských průzkumů veřejného mínění je více lidí pro setrvání v EU. V poslední době se ale oba tábory téměř vyrovnaly."
container = bigContainer.append \div
  ..attr \class \container

lastWidth = null
lastHeight = null
draw = ->
  fullWidth = container.node!offsetWidth
  fullHeight = container.node!offsetHeight
  return if fullWidth is lastWidth and fullHeight is lastHeight
  lastWidth := fullWidth
  lastHeight := fullHeight
  container.html ''
  width = fullWidth - padding.left - padding.right
  height = fullHeight - padding.top - padding.bottom

  svg = container.append \svg
    ..attr \class \graph
    ..attr \width fullWidth
    ..attr \height fullHeight
  drawing = svg.append \g
    ..attr \class \drawing
    ..attr \transform "translate(#{padding.left}, #{padding.top})"
  displayY = if fullWidth < 767 then 53 else maxY

  scaleY = d3.scale.linear!
    ..domain [0 displayY]
    ..range [height, 0]

  scaleX = d3.time.scale!
    ..domain [data.combinedData[*-160].date, data.combinedData[0].date]
    ..range [0 width]

  lines = for field in <[remain leave undecided]>
    data.smoothedData.map -> x: it.date, y: it[field]

  lineGenerator = d3.svg.line!
    ..x -> scaleX it.x
    ..y -> scaleY it.y
  combinedDataToUse = data.combinedData.slice 0, (data.combinedData.length) - 160
  pointsG = drawing.append \g
    ..attr \class \points
  pointsG.selectAll \g .data combinedDataToUse .enter!append \g
    ..attr \class \point
    ..classed \telephone -> it.method == "Telephone Interviews"
    ..attr \transform -> "translate(#{scaleX it.date}, 0)"
    ..append \circle
      ..attr \class \remain
      ..attr \r 3
      ..attr \cy -> scaleY it.remain
    ..append \circle
      ..attr \class \leave
      ..attr \r 3
      ..attr \cy -> scaleY it.leave
    ..append \circle
      ..attr \class \undecided
      ..attr \r 3
      ..attr \cy -> scaleY it.undecided
  fiftyLine = drawing.append \g
    ..attr \transform "translate(0, #{scaleY 50})"
    ..attr \class \fifty
    ..append \line
      ..attr \x2 width
    ..append \text
      ..attr \x width + 10
      ..attr \dy 4
      ..text "50 %"

  trendLineG = drawing.append \g
    ..attr \class \trend

  trendLine = trendLineG.selectAll \path .data lines .enter!append \path
    ..attr \d lineGenerator

  latest = drawing.append \g
    ..attr \class \latest
    ..append \text
      ..attr \class \remain
      ..attr \x width + 10
      ..attr \dy 4
      ..datum data.smoothedData[*-1]
      ..attr \y -> scaleY it.remain
      ..text -> "#{it.remain} % pro setrvání"
    ..append \text
      ..attr \class \leave
      ..attr \x width + 10
      ..attr \dy 4
      ..datum data.smoothedData[*-1]
      ..attr \y -> scaleY it.leave
      ..text -> "#{it.leave} % pro vystoupení"
    ..append \text
      ..attr \class \undecided
      ..attr \x width + 10
      ..attr \dy 4
      ..datum data.smoothedData[*-1]
      ..attr \y -> scaleY it.undecided
      ..text -> "#{it.undecided} % nerozhodnutých"

  months = <[leden únor březen duben květen červen červenec srpen září říjen listopad prosinec]>

  axis = d3.svg.axis!
    ..scale scaleX
    ..innerTickSize 4
    ..outerTickSize 1
    ..tickFormat ->
      str = months[it.getMonth!]
      if it.getMonth! is 0
        str += " " + it.getFullYear!
      str

  svg.append \g
    ..attr \class \axis
    ..attr \transform "translate(#{padding.left}, #{padding.top + height + 10})"
    ..call axis
    ..selectAll \text
      ..attr \dy 10
  points = []
  for datum in combinedDataToUse
    x = scaleX datum.date
    points.push {datum, x, y: scaleY datum.leave}
    points.push {datum, x, y: scaleY datum.remain}
    points.push {datum, x, y: scaleY datum.undecided}


  voronoiPointGenerator = d3.geom.voronoi!
    ..x ~> padding.left + it.x
    ..y ~> padding.top + it.y
    ..clipExtent [[0, 0], [fullWidth, fullHeight]]

  voronois = voronoiPointGenerator points
    .filter -> it and it.length

  tooltipContainer = container.append \div
    ..attr \class \tooltip-container
  tooltip = new ig.GraphTip tooltipContainer
  months2 = <[ledna února března dubna května června července srpna září října listopadu prosince]>
  voronoiSvg = container.append \svg
    ..attr \class \voronoi
    ..attr \width fullWidth
    ..attr \height fullHeight
    ..selectAll \path .data voronois .enter!append \path
      ..attr \d polygon
      ..on \mouseover ({{datum}:point}) ->
        str = "Průzkum společnosti <b>#{datum.pollster}</b><br>"
        str += "Metoda: "
        str += switch datum.method
        | "Telephone Interviews" => "<b>telefonní průzkum</b>"
        | "Online" => "<b>online dotazník</b>"
        | otherwise => "<b>neznámá</b>"
        str += "<br>"
        str += "Počet dotázaných: <b>#{ig.utils.formatNumber datum.sample}</b><br>"
        str += "Datum: <b>#{datum.date.getDate!}. #{months2[datum.date.getMonth!]} #{datum.date.getFullYear!}</b><br>"
        str += "Pro setrvání: <b class='remain'>#{ig.utils.formatNumber datum.remain} %</b><br>"
        str += "Pro vystoupení: <b class='leave'>#{ig.utils.formatNumber datum.leave} %</b><br>"
        str += "Nerozhodnutých: <b class='undecided'>#{ig.utils.formatNumber datum.undecided} %</b><br>"

        tooltip.display do
          (point.x + padding.left)
          (point.y + padding.top - 5)
          str
      ..on \mouseout -> tooltip.hide!
  container.append \div
    ..attr \class \legend
    ..append \ul
      ..attr \class \answer
      ..append \li .html "Pro setrvání"
      ..append \li .html "Pro vystoupení"
      ..append \li .html "Nerozhodnutí"
    ..append \ul
      ..attr \class \type
      ..append \li
        ..html "Telefonní průzkumy"
      ..append \li
        ..html "Ostatní"
    ..append \span
      ..html "Data <a href='https://github.com/ft-interactive/brexit-polling'>Financial Times</a>"

polygon = ->
  "M#{it.join "L"}Z"


draw!
lastBounce = Date.now!
bounceTimeout = null
debounce = (timeout, fn) ->
  now = Date.now!
  if now - lastBounce > timeout
    doAction fn
  else
    clearTimeout bounceTimeout if bounceTimeout
    bounceTimeout := setTimeout do
      -> doAction fn
      timeout - (now - lastBounce)
doAction = (fn) ->
  fn!
  lastBounce := Date.now!
  if bounceTimeout
    clearTimeout bounceTimeout
    bounceTimeout = null

window.addEventListener \resize ->
  <~ debounce 500
  draw!
