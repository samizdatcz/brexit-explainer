 Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#hc-azylanti').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Uprchlíci v Británii',
        },
        subtitle: {
            text: 'Loni na podzim vlna uprchlíků na ostrovy zesílila',
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: ''
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%b %Y}: {point.y} uprchlíků'
        },
            exporting: {
                    enabled: false
                },
           colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        plotOptions: {
            series: {
                    compare: 'value'
                }
        },
        credits: {
            enabled: false,
            href : "https://www.gov.uk/government/publications/immigration-statistics-october-to-december-2015/asylum#long-term-trends-in-asylum-applications-for-main-applicants",
            text : "Zdroj: Home Office"
        },
        series: [{
            name: 'žádosti o azyl (čtvrtletně)',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
[Date.UTC(2001,3,1),19007],
[Date.UTC(2001,6,1),15683],
[Date.UTC(2001,9,1),18617],
[Date.UTC(2001,12,1),17720],
[Date.UTC(2002,3,1),19252],
[Date.UTC(2002,6,1),20091],
[Date.UTC(2002,9,1),22029],
[Date.UTC(2002,12,1),22760],
[Date.UTC(2003,3,1),15856],
[Date.UTC(2003,6,1),10671],
[Date.UTC(2003,9,1),12056],
[Date.UTC(2003,12,1),10824],
[Date.UTC(2004,3,1),8953],
[Date.UTC(2004,6,1),7913],
[Date.UTC(2004,9,1),8615],
[Date.UTC(2004,12,1),8479],
[Date.UTC(2005,3,1),7006],
[Date.UTC(2005,6,1),6214],
[Date.UTC(2005,9,1),6318],
[Date.UTC(2005,12,1),6174],
[Date.UTC(2006,3,1),6453],
[Date.UTC(2006,6,1),5497],
[Date.UTC(2006,9,1),5862],
[Date.UTC(2006,12,1),5796],
[Date.UTC(2007,3,1),5719],
[Date.UTC(2007,6,1),4958],
[Date.UTC(2007,9,1),5885],
[Date.UTC(2007,12,1),6869],
[Date.UTC(2008,3,1),6644],
[Date.UTC(2008,6,1),5830],
[Date.UTC(2008,9,1),6683],
[Date.UTC(2008,12,1),6775],
[Date.UTC(2009,3,1),8428],
[Date.UTC(2009,6,1),6111],
[Date.UTC(2009,9,1),5108],
[Date.UTC(2009,12,1),4840],
[Date.UTC(2010,3,1),4382],
[Date.UTC(2010,6,1),4389],
[Date.UTC(2010,9,1),4486],
[Date.UTC(2010,12,1),4659],
[Date.UTC(2011,3,1),4877],
[Date.UTC(2011,6,1),4801],
[Date.UTC(2011,9,1),4918],
[Date.UTC(2011,12,1),5269],
[Date.UTC(2012,3,1),4838],
[Date.UTC(2012,6,1),4971],
[Date.UTC(2012,9,1),5812],
[Date.UTC(2012,12,1),6222],
[Date.UTC(2013,3,1),5630],
[Date.UTC(2013,6,1),5859],
[Date.UTC(2013,9,1),6094],
[Date.UTC(2013,12,1),6001],
[Date.UTC(2014,3,1),5858],
[Date.UTC(2014,6,1),5562],
[Date.UTC(2014,9,1),6903],
[Date.UTC(2014,12,1),6710],
[Date.UTC(2015,3,1),5955],
[Date.UTC(2015,6,1),6203],
[Date.UTC(2015,9,1),10156],
[Date.UTC(2015,12,1),10100],
],
            marker: {
                enabled: false
            }
        }]
    });
/**
 * Code for regression extracted from jqplot.trendline.js
 *
 * Version: 1.0.0a_r701
 *
 * Copyright (c) 2009-2011 Chris Leonello
 * jqPlot is currently available for use in all personal or commercial projects
 * under both the MIT (http://www.opensource.org/licenses/mit-license.php) and GPL
 * version 2.0 (http://www.gnu.org/licenses/gpl-2.0.html) licenses. This means that you can
 * choose the license that best suits your project and use it accordingly.
 *
 **/

function regression(x, y, typ) {
  var type = (typ == null) ? 'linear' : typ;
  var N = x.length;
  var slope;
  var intercept;
  var SX = 0;
  var SY = 0;
  var SXX = 0;
  var SXY = 0;
  var SYY = 0;
  var Y = [];
  var X = [];

  if (type == 'linear') {
    X = x;
    Y = y;
  }
  else if (type == 'exp' || type == 'exponential') {
    for (var i = 0; i < y.length; i++) {
      // ignore points <= 0, log undefined.
      if (y[i] <= 0) {
        N--;
      }
      else {
        X.push(x[i]);
        Y.push(Math.log(y[i]));
      }
    }
  }

  for (var i = 0; i < N; i++) {
    SX = SX + X[i];
    SY = SY + Y[i];
    SXY = SXY + X[i] * Y[i];
    SXX = SXX + X[i] * X[i];
    SYY = SYY + Y[i] * Y[i];
  }

  slope = (N * SXY - SX * SY) / (N * SXX - SX * SX);
  intercept = (SY - slope * SX) / N;

  return [slope, intercept];
}

function linearRegression(X, Y) {
  var ret;
  ret = regression(X, Y, 'linear');
  return [ret[0], ret[1]];
}

function expRegression(X, Y) {
  var ret;
  var x = X;
  var y = Y;
  ret = regression(x, y, 'exp');
  var base = Math.exp(ret[0]);
  var coeff = Math.exp(ret[1]);
  return [base, coeff];
}

/*
    TODO: this function is quite inefficient.
    Refactor it if there is problem with speed.
 */
function fitData(data, typ) {
  var type = (typ == null) ? 'linear' : typ;
  var ret;
  var res;
  var x = [];
  var y = [];
  var ypred = [];

  for (i = 0; i < data.length; i++) {
    if (data[i] != null && Object.prototype.toString.call(data[i]) === '[object Array]') {
      if (data[i] != null && data[i][0] != null && data[i][1] != null) {
        x.push(data[i][0]);
        y.push(data[i][1]);
      }
    }
    else if(data[i] != null && typeof data[i] === 'number' ){//If type of X axis is category
      x.push(i);
      y.push(data[i]);
    }
    else if(data[i] != null && Object.prototype.toString.call(data[i]) === '[object Object]'){
      if (data[i] != null && data[i].x != null && data[i].y != null) {
        x.push(data[i].x);
        y.push(data[i].y);
      }
    }
  }

  if (type == 'linear') {

    ret = linearRegression(x, y);
    for (var i = 0; i < x.length; i++) {
      res = ret[0] * x[i] + ret[1];
      ypred.push([x[i], res]);
    }

    return {
      data: ypred,
      slope: ret[0],
      intercept: ret[1],
      y: function(x) {
        return (this.slope * x) + this.intercept;
      },
      x: function(y) {
        return (y - this.intercept) / this.slope;
      }
    };
  }
  else if (type == 'exp' || type == 'exponential') {

    ret = expRegression(x, y);
    for (var i = 0; i < x.length; i++) {
      res = ret[1] * Math.pow(ret[0], x[i]);
      ypred.push([x[i], res]);
    }
    ypred.sort();

    return {
      data: ypred,
      base: ret[0],
      coeff: ret[1]
    };
  }
}


$(function () {
    var sourceData = [[2.8,4],[7.7,4],[2.3,5],[4.5,6],[5.2,3],[2.7,5],[5.4,7],[13.1,7],[5.4,7],[8.2,6],[3.9,4],[4.4,5],[6.4,5],[9.2,2],[13.1,4],[32.5,3],[6.1,4],[19.4,3],[6.2,5],[6.9,6],[8.9,3],[8.7,4],[13.9,2],[5.6,6],[6.4,4],[6.9,6],[5.3,6],[14.5,4],[8.2,5],[12.5,6],[17.7,7],[28.4,6],[9.6,7],[11.1,4],[9.8,5],[13.1,7],[8.9,4],[23.6,4],[38.5,4],[17.1,4],[12,4],[18,6],[15.7,3],[11.9,4],[17,3],[5.2,5],[3.1,4],[5.4,4],[4.9,4],[4.8,5],[4.5,5],[7.4,4],[7.7,4],[15.2,4],[7.9,6],[2.8,5],[10.8,4],[8.6,5],[24.3,2],[10.9,4],[10.8,4],[11.1,4],[6.8,3],[6.9,5],[10.9,3],[3.5,5],[2.3,4],[9.4,2],[2.7,3],[4.2,5],[4,3],[3.4,6],[5.9,6],[4.9,5],[10.2,3],[12.8,3],[3.9,4],[3.3,7],[4,3],[20.9,4],[20.4,5],[5.2,5],[15.5,7],[7.4,6],[9.3,3],[16.2,5],[15.7,6],[6.9,3],[10.5,4],[10.8,3],[5.4,4],[4.8,4],[43.5,4],[30.3,4],[38.4,3],[13.8,5],[55.2,1],[14.1,5],[42.4,1],[28.2,4],[48.1,5],[34.5,3],[29.6,4],[39.2,1],[42.5,2],[43.6,2],[44.5,4],[10.1,7],[29,5],[42.9,4],[35.2,3],[50.8,4],[28.2,4],[38.8,1],[33.5,4],[37.6,2],[53.6,4],[35.9,5],[24.2,4],[39.2,1],[19.8,3],[43.4,2],[37.4,4],[35.2,3],[52.9,2],[12.1,5],[11.9,4],[3.6,7],[3.3,4],[5.2,4],[5.6,5],[7.5,5],[6.5,5],[7,4],[8.2,5],[12.5,5],[8.5,5],[5.8,4],[6.7,5],[6.5,6],[6,5],[9.4,6],[5.4,5],[5.3,5],[12.7,3],[5.7,6],[4.3,6],[7.2,6],[13.5,4],[7.3,4],[9.6,5],[5.3,4],[3.4,4],[4.4,3],[3.8,4],[3.9,3],[4.1,5],[6.5,5],[5.5,1],[4.5,4],[3.8,4],[5.6,5],[2.3,4],[3.4,3],[4.9,6],[12.2,2],[3.2,3],[2.2,3],[2.1,4],[2.7,4],[4.4,4],[8.6,5],[4,2],[4.3,4]];
    $('#hc-migrace-preference').highcharts({
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        exporting: {
            enabled: false
        },
        title: {
            text: 'Euroskepticismus/imigrace'
        },
        subtitle: {
            text: 'Anglie a Wales: odmítnutí EU nesouvisí s množstvím přistěhovalců v hrabství'
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Podíl přistěhovalců (%)'
            },
            max: 60,
            tickInterval: 10
        },
        yAxis: {
            title: {
                text: 'Postoj k EU'
            },
                categories: ['', 'Optimistický', '', '', 'Neutrální', '', '', 'Skeptický'],
                labels: {
                    rotation: 270
                }
        },
        legend: {
            enabled: false
        },
        colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        plotOptions: {
            scatter: {
                marker: {
                    radius: 2,
                    fillColor:'grey',
                    symbol: 'circle',
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '',
                    pointFormat: '<b>{series.name}:</b> {point.x} % imigrantů',
                }
            },
            line: {
              enableMouseTracking: false
            }
        },
        series: [{name: 'Hartlepool', data: [[2.8,4]]}, {name: 'Middlesbrough', data: [[7.7,4]]}, {name: 'Redcar and Cleveland', data: [[2.3,5]]}, {name: 'Stockton-on-Tees', data: [[4.5,6]]}, {name: 'Darlington', data: [[5.2,3]]}, {name: 'Halton', data: [[2.7,5]]}, {name: 'Warrington', data: [[5.4,7]]}, {name: 'Blackburn with Darwen', data: [[13.1,7]]}, {name: 'Blackpool', data: [[5.4,7]]}, {name: 'Kingston upon Hull, City of', data: [[8.2,6]]}, {name: 'East Riding of Yorkshire', data: [[3.9,4]]}, {name: 'North East Lincolnshire', data: [[4.4,5]]}, {name: 'North Lincolnshire', data: [[6.4,5]]}, {name: 'York', data: [[9.2,2]]}, {name: 'Derby', data: [[13.1,4]]}, {name: 'Leicester', data: [[32.5,3]]}, {name: 'Rutland', data: [[6.1,4]]}, {name: 'Nottingham', data: [[19.4,3]]}, {name: 'Herefordshire, County of', data: [[6.2,5]]}, {name: 'Telford and Wrekin', data: [[6.9,6]]}, {name: 'Stoke-on-Trent', data: [[8.9,3]]}, {name: 'Bath and North East Somerset', data: [[8.7,4]]}, {name: 'Bristol, City of', data: [[13.9,2]]}, {name: 'North Somerset', data: [[5.6,6]]}, {name: 'South Gloucestershire', data: [[6.4,4]]}, {name: 'Plymouth', data: [[6.9,6]]}, {name: 'Torbay', data: [[5.3,6]]}, {name: 'Bournemouth', data: [[14.5,4]]}, {name: 'Poole', data: [[8.2,5]]}, {name: 'Swindon', data: [[12.5,6]]}, {name: 'Peterborough', data: [[17.7,7]]}, {name: 'Luton', data: [[28.4,6]]}, {name: 'Southend-on-Sea', data: [[9.6,7]]}, {name: 'Thurrock', data: [[11.1,4]]}, {name: 'Medway', data: [[9.8,5]]}, {name: 'Bracknell Forest', data: [[13.1,7]]}, {name: 'West Berkshire', data: [[8.9,4]]}, {name: 'Reading', data: [[23.6,4]]}, {name: 'Slough', data: [[38.5,4]]}, {name: 'Windsor and Maidenhead', data: [[17.1,4]]}, {name: 'Wokingham', data: [[12,4]]}, {name: 'Milton Keynes', data: [[18,6]]}, {name: 'Brighton and Hove', data: [[15.7,3]]}, {name: 'Portsmouth', data: [[11.9,4]]}, {name: 'Southampton', data: [[17,3]]}, {name: 'Isle of Wight', data: [[5.2,5]]}, {name: 'County Durham', data: [[3.1,4]]}, {name: 'Cheshire East', data: [[5.4,4]]}, {name: 'Cheshire West and Chester', data: [[4.9,4]]}, {name: 'Shropshire', data: [[4.8,5]]}, {name: 'Cornwall', data: [[4.5,5]]}, {name: 'Isles of Scilly', data: [[7.4,4]]}, {name: 'Wiltshire', data: [[7.7,4]]}, {name: 'Bedford', data: [[15.2,4]]}, {name: 'Central Bedfordshire', data: [[7.9,6]]}, {name: 'Northumberland', data: [[2.8,5]]}, {name: 'Bolton', data: [[10.8,4]]}, {name: 'Bury', data: [[8.6,5]]}, {name: 'Manchester', data: [[24.3,2]]}, {name: 'Oldham', data: [[10.9,4]]}, {name: 'Rochdale', data: [[10.8,4]]}, {name: 'Salford', data: [[11.1,4]]}, {name: 'Stockport', data: [[6.8,3]]}, {name: 'Tameside', data: [[6.9,5]]}, {name: 'Trafford', data: [[10.9,3]]}, {name: 'Wigan', data: [[3.5,5]]}, {name: 'Knowsley', data: [[2.3,4]]}, {name: 'Liverpool', data: [[9.4,2]]}, {name: 'St. Helens', data: [[2.7,3]]}, {name: 'Sefton', data: [[4.2,5]]}, {name: 'Wirral', data: [[4,3]]}, {name: 'Barnsley', data: [[3.4,6]]}, {name: 'Doncaster', data: [[5.9,6]]}, {name: 'Rotherham', data: [[4.9,5]]}, {name: 'Sheffield', data: [[10.2,3]]}, {name: 'Newcastle upon Tyne', data: [[12.8,3]]}, {name: 'North Tyneside', data: [[3.9,4]]}, {name: 'South Tyneside', data: [[3.3,7]]}, {name: 'Sunderland', data: [[4,3]]}, {name: 'Birmingham', data: [[20.9,4]]}, {name: 'Coventry', data: [[20.4,5]]}, {name: 'Dudley', data: [[5.2,5]]}, {name: 'Sandwell', data: [[15.5,7]]}, {name: 'Solihull', data: [[7.4,6]]}, {name: 'Walsall', data: [[9.3,3]]}, {name: 'Wolverhampton', data: [[16.2,5]]}, {name: 'Bradford', data: [[15.7,6]]}, {name: 'Calderdale', data: [[6.9,3]]}, {name: 'Kirklees', data: [[10.5,4]]}, {name: 'Leeds', data: [[10.8,3]]}, {name: 'Wakefield', data: [[5.4,4]]}, {name: 'Gateshead', data: [[4.8,4]]}, {name: 'City of London', data: [[43.5,4]]}, {name: 'Barking and Dagenham', data: [[30.3,4]]}, {name: 'Barnet', data: [[38.4,3]]}, {name: 'Bexley', data: [[13.8,5]]}, {name: 'Brent', data: [[55.2,1]]}, {name: 'Bromley', data: [[14.1,5]]}, {name: 'Camden', data: [[42.4,1]]}, {name: 'Croydon', data: [[28.2,4]]}, {name: 'Ealing', data: [[48.1,5]]}, {name: 'Enfield', data: [[34.5,3]]}, {name: 'Greenwich', data: [[29.6,4]]}, {name: 'Hackney', data: [[39.2,1]]}, {name: 'Hammersmith and Fulham', data: [[42.5,2]]}, {name: 'Haringey', data: [[43.6,2]]}, {name: 'Harrow', data: [[44.5,4]]}, {name: 'Havering', data: [[10.1,7]]}, {name: 'Hillingdon', data: [[29,5]]}, {name: 'Hounslow', data: [[42.9,4]]}, {name: 'Islington', data: [[35.2,3]]}, {name: 'Kensington and Chelsea', data: [[50.8,4]]}, {name: 'Kingston upon Thames', data: [[28.2,4]]}, {name: 'Lambeth', data: [[38.8,1]]}, {name: 'Lewisham', data: [[33.5,4]]}, {name: 'Merton', data: [[37.6,2]]}, {name: 'Newham', data: [[53.6,4]]}, {name: 'Redbridge', data: [[35.9,5]]}, {name: 'Richmond upon Thames', data: [[24.2,4]]}, {name: 'Southwark', data: [[39.2,1]]}, {name: 'Sutton', data: [[19.8,3]]}, {name: 'Tower Hamlets', data: [[43.4,2]]}, {name: 'Waltham Forest', data: [[37.4,4]]}, {name: 'Wandsworth', data: [[35.2,3]]}, {name: 'Westminster', data: [[52.9,2]]}, {name: 'Buckinghamshire', data: [[12.1,5]]}, {name: 'Cambridgeshire', data: [[11.9,4]]}, {name: 'Cumbria', data: [[3.6,7]]}, {name: 'Derbyshire', data: [[3.3,4]]}, {name: 'Devon', data: [[5.2,4]]}, {name: 'Dorset', data: [[5.6,5]]}, {name: 'East Sussex', data: [[7.5,5]]}, {name: 'Essex', data: [[6.5,5]]}, {name: 'Gloucestershire', data: [[7,4]]}, {name: 'Hampshire', data: [[8.2,5]]}, {name: 'Hertfordshire', data: [[12.5,5]]}, {name: 'Kent', data: [[8.5,5]]}, {name: 'Lancashire', data: [[5.8,4]]}, {name: 'Leicestershire', data: [[6.7,5]]}, {name: 'Lincolnshire', data: [[6.5,6]]}, {name: 'Norfolk', data: [[6,5]]}, {name: 'Northamptonshire', data: [[9.4,6]]}, {name: 'North Yorkshire', data: [[5.4,5]]}, {name: 'Nottinghamshire', data: [[5.3,5]]}, {name: 'Oxfordshire', data: [[12.7,3]]}, {name: 'Somerset', data: [[5.7,6]]}, {name: 'Staffordshire', data: [[4.3,6]]}, {name: 'Suffolk', data: [[7.2,6]]}, {name: 'Surrey', data: [[13.5,4]]}, {name: 'Warwickshire', data: [[7.3,4]]}, {name: 'West Sussex', data: [[9.6,5]]}, {name: 'Worcestershire', data: [[5.3,4]]}, {name: 'Isle of Anglesey', data: [[3.4,4]]}, {name: 'Gwynedd', data: [[4.4,3]]}, {name: 'Conwy', data: [[3.8,4]]}, {name: 'Denbighshire', data: [[3.9,3]]}, {name: 'Flintshire', data: [[4.1,5]]}, {name: 'Wrexham', data: [[6.5,5]]}, {name: 'Ceredigion', data: [[5.5,1]]}, {name: 'Pembrokeshire', data: [[4.5,4]]}, {name: 'Carmarthenshire', data: [[3.8,4]]}, {name: 'Swansea', data: [[5.6,5]]}, {name: 'Neath Port Talbot', data: [[2.3,4]]}, {name: 'Bridgend', data: [[3.4,3]]}, {name: 'Vale of Glamorgan', data: [[4.9,6]]}, {name: 'Cardiff', data: [[12.2,2]]}, {name: 'Rhondda Cynon Taf', data: [[3.2,3]]}, {name: 'Caerphilly', data: [[2.2,3]]}, {name: 'Blaenau Gwent', data: [[2.1,4]]}, {name: 'Torfaen', data: [[2.7,4]]}, {name: 'Monmouthshire', data: [[4.4,4]]}, {name: 'Newport', data: [[8.6,5]]}, {name: 'Powys', data: [[4,2]]}, {name: 'Merthyr Tydfil', data: [[4.3,4]]},
        {
            type: 'line',
            lineWidth: 1,
            marker: { enabled: false },
            data: (function() {
                return fitData(sourceData).data;
            })()
        }],
        credits: {
            enabled: false,
            href : "https://yougov.co.uk/news/2016/02/28/eurosceptic-map-britain/",
            text : "Zdroj: YouGov"
        }
    });
});

$('#hc-eupodpora').highcharts({
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Co radí Britům Evropani?'
    },
    subtitle: {
        text: '60 % občanů EU chce setrvání Britů'
    },
    xAxis: {
        categories: ["Litva","Malta","Portugalsko","Irsko","Rumunsko","Španělsko","Bulharsko","Itálie","Polsko","Estonsko","Maďarsko","Slovensko","Německo","Lotyšsko","Dánsko","Švédsko","Lucembursko","Finsko","Francie","Řecko","Belgie","Chorvatsko","Nizozemsko","Slovinsko","Rakousko","Česko","Kypr"]
    },
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: ''
        }
    },
    legend: {
        reversed: true
    },
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false,
        href : "http://lordashcroftpolls.com/wp-content/uploads/2016/02/LORD-ASHCROFT-POLLS-You-Should-Hear-What-They-Say-About-You-EU28-Poll-Report-Feb-2016.pdf",
        text : "Zdroj: Lord Ashcroft Polls"
    },
    colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: [{name: "Opustit", data: [6,6,7,10,4,6,7,9,6,8,7,7,11,9,13,12,24,11,18,15,13,10,10,8,19,13,19]},
             {name: "Neví", data: [16,18,20,18,26,24,27,24,27,28,30,32,30,33,31,33,21,39,32,35,38,41,42,49,41,47,46]},
             {name: "Zůstat", data: [78,76,74,72,70,70,67,67,67,65,64,61,59,58,56,56,55,50,50,50,49,49,49,43,41,40,35]}
    ]
});
$('#hc-migrace').highcharts({
    title: {
        text: 'Kdo přichází, kdo odchází',
    },
    subtitle: {
        text: 'Přistěhovalců z EU v Británii přibývá, migrace ze zbytku světa slábne',
    },
    xAxis: {
        categories: [1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015]
    },
    yAxis: {
        title: {
            text: 'tisíce lidí'
        },
        plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
        }]
    },
    tooltip: {
        valueSuffix: ' tisíc'
    },
    legend: {
        borderWidth: 0
    },
    exporting: {
                enabled: false
            },
    colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
    credits: {
        enabled: false,
        href : "http://www.ons.gov.uk/peoplepopulationandcommunity/populationandmigration/internationalmigration/bulletins/migrationstatisticsquarterlyreport/february2016",
        text : "Zdroj: Office for National Statistics"
    },
    series: [{
        name: 'imigranti z Evropské unie',
        data: [23,28,18,33,8,6,7,7,15,87,96,104,127,63,58,77,82,82,123,174,172],
        marker: {
            enabled: false
        }
    }, {
        name: 'imigranti ze zemí mimo EU',
        data: [104,88,88,129,179,214,213,234,224,266,198,218,204,187,184,217,204,157,142,194,191],
        marker: {
            enabled: false
        }
    }, {
        name: 'emigranti z Británie',
        data: [-51,-62,-59,-22,-24,-62,-48,-88,-91,-107,-88,-124,-97,-87,-44,-43,-70,-63,-57,-55,-40],
        marker: {
            enabled: false
        },
        color: "grey"
    }]
});
$('#hc-eumigrace').highcharts({
    chart: {
        type: 'line'
    },
    title: {
        text: 'Kdo z EU míří do Británie?',
    },
    subtitle: {
        text: 'Sílí imigrace ze "starých zemí" a Balkánu',
    },
    xAxis: {
        type: 'datetime',
         tickInterval: 31557600000
    },
    yAxis: {
        title: {
            text: 'tisíce lidí'
        },
        min: 0
    },
    tooltip: {
        headerFormat: '<b>{series.name}</b><br>',
        pointFormat: '{point.x:%b %Y}: {point.y} tisíc imigrantů'
    },
        exporting: {
                enabled: false
            },
       colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
    plotOptions: {
        series: {
                compare: 'value'
            }
    },
    credits: {
        enabled: false,
        href : "http://www.ons.gov.uk/peoplepopulationandcommunity/populationandmigration/internationalmigration/bulletins/migrationstatisticsquarterlyreport/february2016",
        text : "Zdroj: Office for National Statistics"
    },
    series: [{
        name: 'staré země EU',
        // Define the data points. All series have a dummy year
        // of 1970/71 in order to be compared on the same x axis. Note
        // that in JavaScript, months start at 0 for January, 1 for February etc.
        data: [[Date.UTC(2005,12,1),48],[Date.UTC(2006,6,1),54],[Date.UTC(2006,12,1),52],[Date.UTC(2007,6,1),63],[Date.UTC(2007,12,1),63],[Date.UTC(2008,6,1),58],[Date.UTC(2008,12,1),83],[Date.UTC(2009,6,1),76],[Date.UTC(2009,12,1),76],[Date.UTC(2010,3,1),81],[Date.UTC(2010,6,1),86],[Date.UTC(2010,9,1),83],[Date.UTC(2010,12,1),76],[Date.UTC(2011,3,1),76],[Date.UTC(2011,6,1),78],[Date.UTC(2011,9,1),79],[Date.UTC(2011,12,1),83],[Date.UTC(2012,3,1),82],[Date.UTC(2012,6,1),82],[Date.UTC(2012,9,1),79],[Date.UTC(2012,12,1),85],[Date.UTC(2013,3,1),92],[Date.UTC(2013,6,1),96],[Date.UTC(2013,9,1),107],[Date.UTC(2013,12,1),104],[Date.UTC(2014,3,1),112],[Date.UTC(2014,6,1),113],[Date.UTC(2014,9,1),124],[Date.UTC(2014,12,1),129],[Date.UTC(2015,3,1),127],[Date.UTC(2015,6,1),133],[Date.UTC(2015,9,1),130]],
        marker: {
            enabled: false
        }
    }, {
        name: 'nové země EU',
        data: [[Date.UTC(2005,12,1),68],[Date.UTC(2006,6,1),65],[Date.UTC(2006,12,1),81],[Date.UTC(2007,6,1),91],[Date.UTC(2007,12,1),103],[Date.UTC(2008,6,1),95],[Date.UTC(2008,12,1),76],[Date.UTC(2009,6,1),71],[Date.UTC(2009,12,1),57],[Date.UTC(2010,3,1),73],[Date.UTC(2010,6,1),72],[Date.UTC(2010,9,1),86],[Date.UTC(2010,12,1),86],[Date.UTC(2011,3,1),82],[Date.UTC(2011,6,1),86],[Date.UTC(2011,9,1),75],[Date.UTC(2011,12,1),77],[Date.UTC(2012,3,1),71],[Date.UTC(2012,6,1),63],[Date.UTC(2012,9,1),59],[Date.UTC(2012,12,1),60],[Date.UTC(2013,3,1),63],[Date.UTC(2013,6,1),66],[Date.UTC(2013,9,1),73],[Date.UTC(2013,12,1),70],[Date.UTC(2014,3,1),68],[Date.UTC(2014,6,1),73],[Date.UTC(2014,9,1),76],[Date.UTC(2014,12,1),80],[Date.UTC(2015,3,1),81],[Date.UTC(2015,6,1),73],[Date.UTC(2015,9,1),69]],
        marker: {
            enabled: false
        }
    }, {
        name: 'Rumunsko a Bulharsko',
        data: [[Date.UTC(2007,6,1),2],[Date.UTC(2007,12,1),5],[Date.UTC(2008,6,1),9],[Date.UTC(2008,12,1),17],[Date.UTC(2009,6,1),14],[Date.UTC(2009,12,1),13],[Date.UTC(2010,3,1),15],[Date.UTC(2010,6,1),15],[Date.UTC(2010,9,1),10],[Date.UTC(2010,12,1),10],[Date.UTC(2011,3,1),8],[Date.UTC(2011,6,1),7],[Date.UTC(2011,9,1),11],[Date.UTC(2011,12,1),13],[Date.UTC(2012,3,1),12],[Date.UTC(2012,6,1),12],[Date.UTC(2012,9,1),10],[Date.UTC(2012,12,1),11],[Date.UTC(2013,3,1),13],[Date.UTC(2013,6,1),20],[Date.UTC(2013,9,1),25],[Date.UTC(2013,12,1),25],[Date.UTC(2014,3,1),30],[Date.UTC(2014,6,1),34],[Date.UTC(2014,9,1),40],[Date.UTC(2014,12,1),49],[Date.UTC(2015,3,1),57],[Date.UTC(2015,6,1),54],[Date.UTC(2015,9,1),55]],
        marker: {
            enabled: false
        }

    }]
});
$('#hc-expats').highcharts({
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Britové v unijních zemích'
    },
    subtitle: {
        text: 'V nových členských zemích Brity nenajdete, s výjimkou Polska'
    },
    xAxis: {
        categories: ["Španělsko","Irsko","Francie","Německo","Itálie","Nizozemsko","Polsko","Belgie","Švédsko","Dánsko","Portugalsko","Řecko","Malta","Rakousko","Maďarsko","Finsko","Lucembursko","Bulharsko","Slovensko","Česko","Litva","Rumunsko","Lotyšsko","Chorvatsko","Slovinsko","Estonsko"],
        title: {
            text: null
        }
    },
    yAxis: {
        title: {
            text: '',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        },
    },
    tooltip: {
        valueSuffix: ' Britů'
    },
    exporting: {
        enabled: false
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    credits: {
        enabled: false,
        href : "http://www.un.org/en/development/desa/population/migration/data/estimates2/data/UN_MigrantStockByOriginAndDestination_2015.xlsx",
        text : "Zdroj: OSN"
    },
    legend: {
        enabled: false
    },
    colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
    series: [{
        name: 'Žije zde',
        data: [308821,254761,185344,103352,64986,49549,34545,27335,24950,18556,17798,17679,12046,11013,6980,6898,6559,5329,4890,4795,3301,3124,1148,670,578,487]
    }]
});
$('#hc-nezamestnanost').highcharts({
        chart: {
            zoomType: 'x'
        },
        title: {
            text: 'Nezaměstnanost v Británii'
        },
        subtitle: {
            text: document.ontouchstart === undefined ?
            'Lidí bez práce je nejméně za 10 let' : ''
        },
        credits: {
            enabled: false,
            href : "https://www.ons.gov.uk/employmentandlabourmarket/peoplenotinwork/unemployment/timeseries/mgsx",
            text : "Zdroj: Office for National Statistics"
        },
        xAxis: {
            type: 'datetime',
            tickInterval: 31557600000
        },
        yAxis: {
            title: {
                text: 'míra nezaměstnanosti (%)'
            },
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        tooltip: {
            valueSuffix: " %"
        },
        colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                    [0, Highcharts.getOptions().colors[0]],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 2,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },

        series: [{
            type: 'line',
            name: 'míra nezaměstnanosti',
            data:

            [
            [Date.UTC(2006,3,1),5.3],
            [Date.UTC(2006,4,1),5.4],
            [Date.UTC(2006,5,1),5.5],
            [Date.UTC(2006,6,1),5.5],
            [Date.UTC(2006,7,1),5.5],
            [Date.UTC(2006,8,1),5.5],
            [Date.UTC(2006,9,1),5.5],
            [Date.UTC(2006,10,1),5.4],
            [Date.UTC(2006,11,1),5.5],
            [Date.UTC(2006,12,1),5.5],
            [Date.UTC(2007,1,1),5.5],
            [Date.UTC(2007,2,1),5.5],
            [Date.UTC(2007,3,1),5.5],
            [Date.UTC(2007,4,1),5.4],
            [Date.UTC(2007,5,1),5.4],
            [Date.UTC(2007,6,1),5.3],
            [Date.UTC(2007,7,1),5.3],
            [Date.UTC(2007,8,1),5.3],
            [Date.UTC(2007,9,1),5.2],
            [Date.UTC(2007,10,1),5.2],
            [Date.UTC(2007,11,1),5.2],
            [Date.UTC(2007,12,1),5.2],
            [Date.UTC(2008,1,1),5.2],
            [Date.UTC(2008,2,1),5.2],
            [Date.UTC(2008,3,1),5.3],
            [Date.UTC(2008,4,1),5.2],
            [Date.UTC(2008,5,1),5.4],
            [Date.UTC(2008,6,1),5.5],
            [Date.UTC(2008,7,1),5.7],
            [Date.UTC(2008,8,1),5.9],
            [Date.UTC(2008,9,1),6.0],
            [Date.UTC(2008,10,1),6.2],
            [Date.UTC(2008,11,1),6.4],
            [Date.UTC(2008,12,1),6.5],
            [Date.UTC(2009,1,1),6.7],
            [Date.UTC(2009,2,1),7.1],
            [Date.UTC(2009,3,1),7.3],
            [Date.UTC(2009,4,1),7.6],
            [Date.UTC(2009,5,1),7.8],
            [Date.UTC(2009,6,1),7.9],
            [Date.UTC(2009,7,1),7.9],
            [Date.UTC(2009,8,1),7.8],
            [Date.UTC(2009,9,1),7.9],
            [Date.UTC(2009,10,1),7.8],
            [Date.UTC(2009,11,1),7.8],
            [Date.UTC(2009,12,1),7.7],
            [Date.UTC(2010,1,1),7.9],
            [Date.UTC(2010,2,1),8.0],
            [Date.UTC(2010,3,1),8.0],
            [Date.UTC(2010,4,1),7.9],
            [Date.UTC(2010,5,1),7.9],
            [Date.UTC(2010,6,1),7.8],
            [Date.UTC(2010,7,1),7.8],
            [Date.UTC(2010,8,1),7.8],
            [Date.UTC(2010,9,1),7.9],
            [Date.UTC(2010,10,1),7.9],
            [Date.UTC(2010,11,1),7.9],
            [Date.UTC(2010,12,1),7.9],
            [Date.UTC(2011,1,1),7.8],
            [Date.UTC(2011,2,1),7.8],
            [Date.UTC(2011,3,1),7.7],
            [Date.UTC(2011,4,1),7.8],
            [Date.UTC(2011,5,1),7.9],
            [Date.UTC(2011,6,1),8.0],
            [Date.UTC(2011,7,1),8.2],
            [Date.UTC(2011,8,1),8.3],
            [Date.UTC(2011,9,1),8.4],
            [Date.UTC(2011,10,1),8.5],
            [Date.UTC(2011,11,1),8.4],
            [Date.UTC(2011,12,1),8.3],
            [Date.UTC(2012,1,1),8.3],
            [Date.UTC(2012,2,1),8.2],
            [Date.UTC(2012,3,1),8.2],
            [Date.UTC(2012,4,1),8.1],
            [Date.UTC(2012,5,1),8.0],
            [Date.UTC(2012,6,1),8.1],
            [Date.UTC(2012,7,1),7.9],
            [Date.UTC(2012,8,1),7.9],
            [Date.UTC(2012,9,1),7.9],
            [Date.UTC(2012,10,1),7.8],
            [Date.UTC(2012,11,1),7.8],
            [Date.UTC(2012,12,1),7.8],
            [Date.UTC(2013,1,1),8.0],
            [Date.UTC(2013,2,1),7.8],
            [Date.UTC(2013,3,1),7.8],
            [Date.UTC(2013,4,1),7.8],
            [Date.UTC(2013,5,1),7.7],
            [Date.UTC(2013,6,1),7.7],
            [Date.UTC(2013,7,1),7.7],
            [Date.UTC(2013,8,1),7.6],
            [Date.UTC(2013,9,1),7.4],
            [Date.UTC(2013,10,1),7.2],
            [Date.UTC(2013,11,1),7.2],
            [Date.UTC(2013,12,1),7.2],
            [Date.UTC(2014,1,1),6.9],
            [Date.UTC(2014,2,1),6.8],
            [Date.UTC(2014,3,1),6.6],
            [Date.UTC(2014,4,1),6.4],
            [Date.UTC(2014,5,1),6.3],
            [Date.UTC(2014,6,1),6.1],
            [Date.UTC(2014,7,1),6.0],
            [Date.UTC(2014,8,1),6.0],
            [Date.UTC(2014,9,1),6.0],
            [Date.UTC(2014,10,1),5.9],
            [Date.UTC(2014,11,1),5.7],
            [Date.UTC(2014,12,1),5.7],
            [Date.UTC(2015,1,1),5.6],
            [Date.UTC(2015,2,1),5.6],
            [Date.UTC(2015,3,1),5.5],
            [Date.UTC(2015,4,1),5.6],
            [Date.UTC(2015,5,1),5.6],
            [Date.UTC(2015,6,1),5.5],
            [Date.UTC(2015,7,1),5.4],
            [Date.UTC(2015,8,1),5.3],
            [Date.UTC(2015,9,1),5.2],
            [Date.UTC(2015,10,1),5.1],
            [Date.UTC(2015,11,1),5.1],
            [Date.UTC(2015,12,1),5.1],
            [Date.UTC(2016,1,1),5.1],
            [Date.UTC(2016,2,1),5.1]
            ]

        }]
    });
    $('#hc-azylanti').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Uprchlíci v Británii',
        },
        subtitle: {
            text: 'Loni na podzim vlna uprchlíků na ostrovy zesílila',
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: ''
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%b %Y}: {point.y} uprchlíků'
        },
            exporting: {
                    enabled: false
                },
           colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        plotOptions: {
            series: {
                    compare: 'value'
                }
        },
        credits: {
            enabled: false,
            href : "https://www.gov.uk/government/publications/immigration-statistics-october-to-december-2015/asylum#long-term-trends-in-asylum-applications-for-main-applicants",
            text : "Zdroj: Home Office"
        },
        series: [{
            name: 'žádosti o azyl (čtvrtletně)',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
[Date.UTC(2001,3,1),19007],
[Date.UTC(2001,6,1),15683],
[Date.UTC(2001,9,1),18617],
[Date.UTC(2001,12,1),17720],
[Date.UTC(2002,3,1),19252],
[Date.UTC(2002,6,1),20091],
[Date.UTC(2002,9,1),22029],
[Date.UTC(2002,12,1),22760],
[Date.UTC(2003,3,1),15856],
[Date.UTC(2003,6,1),10671],
[Date.UTC(2003,9,1),12056],
[Date.UTC(2003,12,1),10824],
[Date.UTC(2004,3,1),8953],
[Date.UTC(2004,6,1),7913],
[Date.UTC(2004,9,1),8615],
[Date.UTC(2004,12,1),8479],
[Date.UTC(2005,3,1),7006],
[Date.UTC(2005,6,1),6214],
[Date.UTC(2005,9,1),6318],
[Date.UTC(2005,12,1),6174],
[Date.UTC(2006,3,1),6453],
[Date.UTC(2006,6,1),5497],
[Date.UTC(2006,9,1),5862],
[Date.UTC(2006,12,1),5796],
[Date.UTC(2007,3,1),5719],
[Date.UTC(2007,6,1),4958],
[Date.UTC(2007,9,1),5885],
[Date.UTC(2007,12,1),6869],
[Date.UTC(2008,3,1),6644],
[Date.UTC(2008,6,1),5830],
[Date.UTC(2008,9,1),6683],
[Date.UTC(2008,12,1),6775],
[Date.UTC(2009,3,1),8428],
[Date.UTC(2009,6,1),6111],
[Date.UTC(2009,9,1),5108],
[Date.UTC(2009,12,1),4840],
[Date.UTC(2010,3,1),4382],
[Date.UTC(2010,6,1),4389],
[Date.UTC(2010,9,1),4486],
[Date.UTC(2010,12,1),4659],
[Date.UTC(2011,3,1),4877],
[Date.UTC(2011,6,1),4801],
[Date.UTC(2011,9,1),4918],
[Date.UTC(2011,12,1),5269],
[Date.UTC(2012,3,1),4838],
[Date.UTC(2012,6,1),4971],
[Date.UTC(2012,9,1),5812],
[Date.UTC(2012,12,1),6222],
[Date.UTC(2013,3,1),5630],
[Date.UTC(2013,6,1),5859],
[Date.UTC(2013,9,1),6094],
[Date.UTC(2013,12,1),6001],
[Date.UTC(2014,3,1),5858],
[Date.UTC(2014,6,1),5562],
[Date.UTC(2014,9,1),6903],
[Date.UTC(2014,12,1),6710],
[Date.UTC(2015,3,1),5955],
[Date.UTC(2015,6,1),6203],
[Date.UTC(2015,9,1),10156],
[Date.UTC(2015,12,1),10100],
],
            marker: {
                enabled: false
            }
        }]
    });
$('#hc-eurozpocet').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Příjemci a dárci'
        },
        subtitle: {
            text: 'Britové jsou třetím největším sponzorem Evropské unie'
        },
        xAxis: {
            categories: ["Německo","Francie","Británie","Nizozemsko","Itálie","Švédsko","Belgie","Rakousko","Dánsko","Finsko","Irsko","Lucembursko","Kypr","Chrvatsko","Malta","Estonsko","Slovinsko","Lotyšsko","Slovensko","Španělsko","Litva","Bulharsko","Česko","Portugalsko","Rumunsko","Řecko","Maďarsko","Polsko"]
        },
        yAxis: {
            title: {
                text: 'miliardy eur',
                align: 'low'
            }
        },
        tooltip: {
            valueSuffix: ' miliard eur'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    format: "{point.y:.1f}"
                },
                negativeColor: "rgb(228,26,28)"
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false,
            href : "http://ec.europa.eu/budget/figures/interactive/index_en.cfm",
            text : "Zdroj: Evropská komise"
        },
        exporting: {
                    enabled: false
                },
        colors: ['rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        series: [{
            name: 'Celková bilance s EU',
            data: [-15.501,-7.164,-4.298,-4.711,-4.467,-2.312,-1.478,-1.240,-0.836,-0.809,+0.038,+0.080,+0.114,+0.173,+0.179,+0.473,+0.794,+0.799,+1.010,+1.090,+1.543,+1.824,+3.004,+3.211,+4.519,+5.162,+5.681,+13.748]
        }]
    });
$('#hc-libra').highcharts({
        chart: {
            zoomType: 'x'
        },
        title: {
            text: 'Libra oslabuje'
        },
        credits: {
            enabled: false,
            href : "https://www.google.com/finance",
            text : "Zdroj: Google Finance"
        },
        xAxis: {
            type: 'datetime',
            tickInterval: 86400000
        },
        yAxis: {
            title: {
                text: 'dolarů za libru'
            },
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        colors: ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(255,255,51)','rgb(166,86,40)','rgb(247,129,191)'],
        plotOptions: {

        },

        series: [{
            type: 'line',
            name: 'dolarů za libru',
            data:

            [[Date.UTC(2015,5,19),1.5513],
                [Date.UTC(2015,5,20),1.554135],
                [Date.UTC(2015,5,21),1.565825],
                [Date.UTC(2015,5,22),1.54905],
                [Date.UTC(2015,5,23),1.54905],
                [Date.UTC(2015,5,24),1.54724],
                [Date.UTC(2015,5,25),1.54703],
                [Date.UTC(2015,5,26),1.53955],
                [Date.UTC(2015,5,27),1.53487],
                [Date.UTC(2015,5,28),1.532],
                [Date.UTC(2015,5,29),1.52945],
                [Date.UTC(2015,5,30),1.52945],
                [Date.UTC(2015,5,31),1.52854],
                [Date.UTC(2015,6,1),1.51969],
                [Date.UTC(2015,6,2),1.5338],
                [Date.UTC(2015,6,3),1.532705],
                [Date.UTC(2015,6,4),1.53614],
                [Date.UTC(2015,6,5),1.52765],
                [Date.UTC(2015,6,6),1.52765],
                [Date.UTC(2015,6,7),1.52636],
                [Date.UTC(2015,6,8),1.5343],
                [Date.UTC(2015,6,9),1.53865],
                [Date.UTC(2015,6,10),1.5497],
                [Date.UTC(2015,6,11),1.5516],
                [Date.UTC(2015,6,12),1.556],
                [Date.UTC(2015,6,13),1.556],
                [Date.UTC(2015,6,14),1.5561],
                [Date.UTC(2015,6,15),1.55983],
                [Date.UTC(2015,6,16),1.5638],
                [Date.UTC(2015,6,17),1.5848],
                [Date.UTC(2015,6,18),1.587855],
                [Date.UTC(2015,6,19),1.5881],
                [Date.UTC(2015,6,20),1.5881],
                [Date.UTC(2015,6,21),1.58717],
                [Date.UTC(2015,6,22),1.5827],
                [Date.UTC(2015,6,23),1.5738],
                [Date.UTC(2015,6,24),1.5705],
                [Date.UTC(2015,6,25),1.57452],
                [Date.UTC(2015,6,26),1.5747],
                [Date.UTC(2015,6,27),1.5747],
                [Date.UTC(2015,6,28),1.569055],
                [Date.UTC(2015,6,29),1.57295],
                [Date.UTC(2015,6,30),1.5686],
                [Date.UTC(2015,7,1),1.56048],
                [Date.UTC(2015,7,2),1.56085],
                [Date.UTC(2015,7,3),1.5571],
                [Date.UTC(2015,7,4),1.5571],
                [Date.UTC(2015,7,5),1.555],
                [Date.UTC(2015,7,6),1.559875],
                [Date.UTC(2015,7,7),1.5461],
                [Date.UTC(2015,7,8),1.536675],
                [Date.UTC(2015,7,9),1.5371],
                [Date.UTC(2015,7,10),1.55175],
                [Date.UTC(2015,7,11),1.55175],
                [Date.UTC(2015,7,12),1.5515],
                [Date.UTC(2015,7,13),1.54779],
                [Date.UTC(2015,7,14),1.5635],
                [Date.UTC(2015,7,15),1.5626],
                [Date.UTC(2015,7,16),1.56116],
                [Date.UTC(2015,7,17),1.5603],
                [Date.UTC(2015,7,18),1.5603],
                [Date.UTC(2015,7,19),1.560395],
                [Date.UTC(2015,7,20),1.556925],
                [Date.UTC(2015,7,21),1.555445],
                [Date.UTC(2015,7,22),1.561285],
                [Date.UTC(2015,7,23),1.55175],
                [Date.UTC(2015,7,24),1.5509],
                [Date.UTC(2015,7,25),1.5509],
                [Date.UTC(2015,7,26),1.55225],
                [Date.UTC(2015,7,27),1.556265],
                [Date.UTC(2015,7,28),1.56095],
                [Date.UTC(2015,7,29),1.560005],
                [Date.UTC(2015,7,30),1.56041],
                [Date.UTC(2015,7,31),1.5622],
                [Date.UTC(2015,8,1),1.5622],
                [Date.UTC(2015,8,2),1.56218],
                [Date.UTC(2015,8,3),1.55895],
                [Date.UTC(2015,8,4),1.55662],
                [Date.UTC(2015,8,5),1.56038],
                [Date.UTC(2015,8,6),1.551285],
                [Date.UTC(2015,8,7),1.54945],
                [Date.UTC(2015,8,8),1.54945],
                [Date.UTC(2015,8,9),1.548925],
                [Date.UTC(2015,8,10),1.5585],
                [Date.UTC(2015,8,11),1.55825],
                [Date.UTC(2015,8,12),1.56085],
                [Date.UTC(2015,8,13),1.56141],
                [Date.UTC(2015,8,14),1.5645],
                [Date.UTC(2015,8,15),1.5645],
                [Date.UTC(2015,8,16),1.56505],
                [Date.UTC(2015,8,17),1.558115],
                [Date.UTC(2015,8,18),1.56635],
                [Date.UTC(2015,8,19),1.568255],
                [Date.UTC(2015,8,20),1.5689],
                [Date.UTC(2015,8,21),1.56945],
                [Date.UTC(2015,8,22),1.56945],
                [Date.UTC(2015,8,23),1.5676],
                [Date.UTC(2015,8,24),1.57615],
                [Date.UTC(2015,8,25),1.56982],
                [Date.UTC(2015,8,26),1.5479],
                [Date.UTC(2015,8,27),1.541045],
                [Date.UTC(2015,8,28),1.53915],
                [Date.UTC(2015,8,29),1.53915],
                [Date.UTC(2015,8,30),1.54155],
                [Date.UTC(2015,8,31),1.53621],
                [Date.UTC(2015,9,1),1.5304],
                [Date.UTC(2015,9,2),1.530505],
                [Date.UTC(2015,9,3),1.52575],
                [Date.UTC(2015,9,4),1.51695],
                [Date.UTC(2015,9,5),1.51695],
                [Date.UTC(2015,9,6),1.51975],
                [Date.UTC(2015,9,7),1.52755],
                [Date.UTC(2015,9,8),1.539795],
                [Date.UTC(2015,9,9),1.53567],
                [Date.UTC(2015,9,10),1.543695],
                [Date.UTC(2015,9,11),1.5428],
                [Date.UTC(2015,9,12),1.5428],
                [Date.UTC(2015,9,13),1.54455],
                [Date.UTC(2015,9,14),1.54255],
                [Date.UTC(2015,9,15),1.534325],
                [Date.UTC(2015,9,16),1.5516],
                [Date.UTC(2015,9,17),1.556645],
                [Date.UTC(2015,9,18),1.55305],
                [Date.UTC(2015,9,19),1.55305],
                [Date.UTC(2015,9,20),1.55285],
                [Date.UTC(2015,9,21),1.55075],
                [Date.UTC(2015,9,22),1.53515],
                [Date.UTC(2015,9,23),1.52487],
                [Date.UTC(2015,9,24),1.5229],
                [Date.UTC(2015,9,25),1.518],
                [Date.UTC(2015,9,26),1.518],
                [Date.UTC(2015,9,27),1.5203],
                [Date.UTC(2015,9,28),1.5163],
                [Date.UTC(2015,9,29),1.515515],
                [Date.UTC(2015,9,30),1.51205],
                [Date.UTC(2015,10,1),1.513755],
                [Date.UTC(2015,10,2),1.5178],
                [Date.UTC(2015,10,3),1.5178],
                [Date.UTC(2015,10,4),1.5187],
                [Date.UTC(2015,10,5),1.51535],
                [Date.UTC(2015,10,6),1.52256],
                [Date.UTC(2015,10,7),1.5315],
                [Date.UTC(2015,10,8),1.5351],
                [Date.UTC(2015,10,9),1.53265],
                [Date.UTC(2015,10,10),1.53265],
                [Date.UTC(2015,10,11),1.532205],
                [Date.UTC(2015,10,12),1.533585],
                [Date.UTC(2015,10,13),1.525495],
                [Date.UTC(2015,10,14),1.54765],
                [Date.UTC(2015,10,15),1.547275],
                [Date.UTC(2015,10,16),1.5439],
                [Date.UTC(2015,10,17),1.5439],
                [Date.UTC(2015,10,18),1.5435],
                [Date.UTC(2015,10,19),1.54705],
                [Date.UTC(2015,10,20),1.544605],
                [Date.UTC(2015,10,21),1.5424],
                [Date.UTC(2015,10,22),1.53835],
                [Date.UTC(2015,10,23),1.5314],
                [Date.UTC(2015,10,24),1.5314],
                [Date.UTC(2015,10,25),1.531415],
                [Date.UTC(2015,10,26),1.53536],
                [Date.UTC(2015,10,27),1.531105],
                [Date.UTC(2015,10,28),1.52671],
                [Date.UTC(2015,10,29),1.5321],
                [Date.UTC(2015,10,30),1.5458],
                [Date.UTC(2015,10,31),1.5458],
                [Date.UTC(2015,11,1),1.54401],
                [Date.UTC(2015,11,2),1.542195],
                [Date.UTC(2015,11,3),1.5418],
                [Date.UTC(2015,11,4),1.538825],
                [Date.UTC(2015,11,5),1.520935],
                [Date.UTC(2015,11,6),1.50495],
                [Date.UTC(2015,11,7),1.50495],
                [Date.UTC(2015,11,8),1.505255],
                [Date.UTC(2015,11,9),1.511885],
                [Date.UTC(2015,11,10),1.5137],
                [Date.UTC(2015,11,11),1.522235],
                [Date.UTC(2015,11,12),1.523],
                [Date.UTC(2015,11,13),1.5232],
                [Date.UTC(2015,11,14),1.5232],
                [Date.UTC(2015,11,15),1.5217],
                [Date.UTC(2015,11,16),1.52013],
                [Date.UTC(2015,11,17),1.521675],
                [Date.UTC(2015,11,18),1.523855],
                [Date.UTC(2015,11,19),1.528625],
                [Date.UTC(2015,11,20),1.51915],
                [Date.UTC(2015,11,21),1.51915],
                [Date.UTC(2015,11,22),1.518855],
                [Date.UTC(2015,11,23),1.51257],
                [Date.UTC(2015,11,24),1.507985],
                [Date.UTC(2015,11,25),1.512305],
                [Date.UTC(2015,11,26),1.509865],
                [Date.UTC(2015,11,27),1.50315],
                [Date.UTC(2015,11,28),1.50315],
                [Date.UTC(2015,11,29),1.503685],
                [Date.UTC(2015,11,30),1.505675],
                [Date.UTC(2015,12,1),1.507645],
                [Date.UTC(2015,12,2),1.49471],
                [Date.UTC(2015,12,3),1.51332],
                [Date.UTC(2015,12,4),1.51115],
                [Date.UTC(2015,12,5),1.51115],
                [Date.UTC(2015,12,6),1.51097],
                [Date.UTC(2015,12,7),1.504865],
                [Date.UTC(2015,12,8),1.50089],
                [Date.UTC(2015,12,9),1.518075],
                [Date.UTC(2015,12,10),1.51604],
                [Date.UTC(2015,12,11),1.5172],
                [Date.UTC(2015,12,12),1.5172],
                [Date.UTC(2015,12,13),1.51964],
                [Date.UTC(2015,12,14),1.515795],
                [Date.UTC(2015,12,15),1.50448],
                [Date.UTC(2015,12,16),1.49752],
                [Date.UTC(2015,12,17),1.492905],
                [Date.UTC(2015,12,18),1.4892],
                [Date.UTC(2015,12,19),1.4892],
                [Date.UTC(2015,12,20),1.490715],
                [Date.UTC(2015,12,21),1.48935],
                [Date.UTC(2015,12,22),1.48208],
                [Date.UTC(2015,12,23),1.4874],
                [Date.UTC(2015,12,24),1.49229],
                [Date.UTC(2015,12,25),1.4838],
                [Date.UTC(2015,12,26),1.4838],
                [Date.UTC(2015,12,27),1.49238],
                [Date.UTC(2015,12,28),1.489035],
                [Date.UTC(2015,12,29),1.482105],
                [Date.UTC(2015,12,30),1.481715],
                [Date.UTC(2015,12,31),1.47475],
                [Date.UTC(2016,1,1),1.47755],
                [Date.UTC(2016,1,2),1.47755],
                [Date.UTC(2016,1,3),1.473515],
                [Date.UTC(2016,1,4),1.471215],
                [Date.UTC(2016,1,5),1.467395],
                [Date.UTC(2016,1,6),1.4629],
                [Date.UTC(2016,1,7),1.46233],
                [Date.UTC(2016,1,8),1.45205],
                [Date.UTC(2016,1,9),1.45205],
                [Date.UTC(2016,1,10),1.452155],
                [Date.UTC(2016,1,11),1.454325],
                [Date.UTC(2016,1,12),1.443535],
                [Date.UTC(2016,1,13),1.440785],
                [Date.UTC(2016,1,14),1.4423],
                [Date.UTC(2016,1,15),1.42565],
                [Date.UTC(2016,1,16),1.42565],
                [Date.UTC(2016,1,17),1.427265],
                [Date.UTC(2016,1,18),1.42508],
                [Date.UTC(2016,1,19),1.41793],
                [Date.UTC(2016,1,20),1.41996],
                [Date.UTC(2016,1,21),1.42257],
                [Date.UTC(2016,1,22),1.4267],
                [Date.UTC(2016,1,23),1.4267],
                [Date.UTC(2016,1,24),1.425465],
                [Date.UTC(2016,1,25),1.42404],
                [Date.UTC(2016,1,26),1.434195],
                [Date.UTC(2016,1,27),1.42442],
                [Date.UTC(2016,1,28),1.435885],
                [Date.UTC(2016,1,29),1.4244],
                [Date.UTC(2016,1,30),1.4244],
                [Date.UTC(2016,1,31),1.424415],
                [Date.UTC(2016,2,1),1.442515],
                [Date.UTC(2016,2,2),1.440745],
                [Date.UTC(2016,2,3),1.4585],
                [Date.UTC(2016,2,4),1.457855],
                [Date.UTC(2016,2,5),1.4503],
                [Date.UTC(2016,2,6),1.4503],
                [Date.UTC(2016,2,7),1.4508],
                [Date.UTC(2016,2,8),1.443445],
                [Date.UTC(2016,2,9),1.44635],
                [Date.UTC(2016,2,10),1.45222],
                [Date.UTC(2016,2,11),1.44788],
                [Date.UTC(2016,2,12),1.4506],
                [Date.UTC(2016,2,13),1.4506],
                [Date.UTC(2016,2,14),1.451995],
                [Date.UTC(2016,2,15),1.4454],
                [Date.UTC(2016,2,16),1.43025],
                [Date.UTC(2016,2,17),1.428165],
                [Date.UTC(2016,2,18),1.43293],
                [Date.UTC(2016,2,19),1.4406],
                [Date.UTC(2016,2,20),1.4406],
                [Date.UTC(2016,2,21),1.426265],
                [Date.UTC(2016,2,22),1.414885],
                [Date.UTC(2016,2,23),1.40089],
                [Date.UTC(2016,2,24),1.392955],
                [Date.UTC(2016,2,25),1.396435],
                [Date.UTC(2016,2,26),1.38725],
                [Date.UTC(2016,2,27),1.38725],
                [Date.UTC(2016,2,28),1.386175],
                [Date.UTC(2016,2,29),1.3927],
                [Date.UTC(2016,3,1),1.39604],
                [Date.UTC(2016,3,2),1.407875],
                [Date.UTC(2016,3,3),1.41721],
                [Date.UTC(2016,3,4),1.42335],
                [Date.UTC(2016,3,5),1.42335],
                [Date.UTC(2016,3,6),1.421045],
                [Date.UTC(2016,3,7),1.425905],
                [Date.UTC(2016,3,8),1.42051],
                [Date.UTC(2016,3,9),1.42058],
                [Date.UTC(2016,3,10),1.42842],
                [Date.UTC(2016,3,11),1.415],
                [Date.UTC(2016,3,12),1.415],
                [Date.UTC(2016,3,13),1.43789],
                [Date.UTC(2016,3,14),1.428035],
                [Date.UTC(2016,3,15),1.415565],
                [Date.UTC(2016,3,16),1.425145],
                [Date.UTC(2016,3,17),1.44744],
                [Date.UTC(2016,3,18),1.4471],
                [Date.UTC(2016,3,19),1.4471],
                [Date.UTC(2016,3,20),1.44618],
                [Date.UTC(2016,3,21),1.43664],
                [Date.UTC(2016,3,22),1.422295],
                [Date.UTC(2016,3,23),1.41155],
                [Date.UTC(2016,3,24),1.412945],
                [Date.UTC(2016,3,25),1.4133],
                [Date.UTC(2016,3,26),1.4133],
                [Date.UTC(2016,3,27),1.4128],
                [Date.UTC(2016,3,28),1.4261],
                [Date.UTC(2016,3,29),1.43845],
                [Date.UTC(2016,3,30),1.43735],
                [Date.UTC(2016,3,31),1.43645],
                [Date.UTC(2016,4,1),1.42295],
                [Date.UTC(2016,4,2),1.42295],
                [Date.UTC(2016,4,3),1.42244],
                [Date.UTC(2016,4,4),1.426275],
                [Date.UTC(2016,4,5),1.41545],
                [Date.UTC(2016,4,6),1.41315],
                [Date.UTC(2016,4,7),1.4053],
                [Date.UTC(2016,4,8),1.4125],
                [Date.UTC(2016,4,9),1.4125],
                [Date.UTC(2016,4,10),1.41285],
                [Date.UTC(2016,4,11),1.423245],
                [Date.UTC(2016,4,12),1.4267],
                [Date.UTC(2016,4,13),1.4189],
                [Date.UTC(2016,4,14),1.4143],
                [Date.UTC(2016,4,15),1.4202],
                [Date.UTC(2016,4,16),1.4202],
                [Date.UTC(2016,4,17),1.418075],
                [Date.UTC(2016,4,18),1.4284],
                [Date.UTC(2016,4,19),1.43855],
                [Date.UTC(2016,4,20),1.4336],
                [Date.UTC(2016,4,21),1.43222],
                [Date.UTC(2016,4,22),1.4403],
                [Date.UTC(2016,4,23),1.4403],
                [Date.UTC(2016,4,24),1.4445],
                [Date.UTC(2016,4,25),1.44865],
                [Date.UTC(2016,4,26),1.4573],
                [Date.UTC(2016,4,27),1.45385],
                [Date.UTC(2016,4,28),1.4611],
                [Date.UTC(2016,4,29),1.461],
                [Date.UTC(2016,4,30),1.461],
                [Date.UTC(2016,5,1),1.460085],
                [Date.UTC(2016,5,2),1.46635],
                [Date.UTC(2016,5,3),1.45477],
                [Date.UTC(2016,5,4),1.4502],
                [Date.UTC(2016,5,5),1.44935],
                [Date.UTC(2016,5,6),1.44305],
                [Date.UTC(2016,5,7),1.44305],
                [Date.UTC(2016,5,8),1.4427],
                [Date.UTC(2016,5,9),1.44139],
                [Date.UTC(2016,5,10),1.4449],
                [Date.UTC(2016,5,11),1.44355],
                [Date.UTC(2016,5,12),1.44455],
                [Date.UTC(2016,5,13),1.4359],
                [Date.UTC(2016,5,14),1.4359],
                [Date.UTC(2016,5,15),1.43508],
                [Date.UTC(2016,5,16),1.444335],
                [Date.UTC(2016,5,17),1.4455],
                [Date.UTC(2016,5,18),1.45955]]

        }]
    });
